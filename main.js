//Load express library
var express = require("express");

//Create instance of express application
var app = express();

//Specify port
var port = parseInt(process.argv[2]) || 3000;

//Assign port and bind port
app.listen(port, function() {
    console.log("Application started on %d port", port);
});

//Routing
app.use(express.static(__dirname + "/public"));

app.use(express.static(__dirname + "/bower_components"));

app.get('/submitForm', function (req, res) {

    console.log(req.query);
    res.send('hello world');
});
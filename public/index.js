//IIF
(function() {
    //Creates an instance of Angular
    var app = angular.module("app", []);

    //Create a function
    var Ctrl = function() {
        var viewCtrl = this;
    
    //Define a model in the controller
        viewCtrl.email ="";
        viewCtrl.password = "";
        viewCtrl.name ="";
        viewCtrl.gender = null;
        viewCtrl.date = null;
        viewCtrl.address = "";
        viewCtrl.country = "";
        viewCtrl.number = "";
        viewCtrl.comments = "";
    
        viewCtrl.resetForm = function() {
            viewCtrl.email ="";
            viewCtrl.password = "";
            viewCtrl.name ="";
            viewCtrl.gender = null;
            viewCtrl.date = null;
            viewCtrl.address = "";
            viewCtrl.country = "";
            viewCtrl.number = "";
            viewCtrl.comments = "";


        };

        viewCtrl.submitForm = function() {
            alert("This form has been submitted");

            $http({
            url: "/submitForm", 
            method: "GET",
            params: { 
                email: viewCtrl.email,
                password: viewCtrl.password,
                name: viewCtrl.name,
                gender: viewCtrl.gender,
                date: viewCtrl.date,
                address: viewCtrl.address,
                country: viewCtrl.country,
                number: viewCtrl.number

            }});
        }
    } // End Ctrl function
 
    //Makes controller
    app.controller("Ctrl", Ctrl);

})();
